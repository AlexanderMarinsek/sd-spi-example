
/* Include from boards > arduino-zero > include */
#include "arduino_pinmap.h"
/* Include from drivers > include > periph */
#include "periph/gpio.h"
#include "periph/adc.h"
#include "xtimer.h"

#include <stdio.h>

#include "data_logger/data_logger.h"

#include <inttypes.h>

int main (void) {

	// LED
	gpio_init (ARDUINO_PIN_13, GPIO_OUT);

	init_data_log();

	char mystr[] = "abcd1234\n";

	write_data_log(mystr, strlen(mystr));

	while (1) {
		gpio_clear (ARDUINO_PIN_13);
		xtimer_usleep(1000000);

		gpio_set (ARDUINO_PIN_13);
		xtimer_usleep(1000000);
		write_data_log(mystr, strlen(mystr));
		write_data_log(mystr, strlen(mystr));
		write_data_log(mystr, strlen(mystr));
		write_data_log(mystr, strlen(mystr));
		write_data_log(mystr, strlen(mystr));
	}

	return 0;
}
