#include "adc_rhomb_zero.h"

#include <stdint.h>
#include <stdio.h>
#include "cpu.h"
#include "periph/gpio.h"
#include "periph/adc.h"
#include "periph_conf.h"
#include "mutex.h"

/* ADC 0 device configuration */
#define ADC_0_DEV                          ADC
#define ADC_0_IRQ                          ADC_IRQn


/* Prototypes */
static int _adc_syncing(void);


static uint8_t adc_state = ADC_STATE_DISABLED;

static mutex_t _lock = MUTEX_INIT;


static inline void _prep(void)
{
    mutex_lock(&_lock);
}


static inline void _done(void)
{
    mutex_unlock(&_lock);
}


static int _adc_syncing(void)
{
    if (ADC_0_DEV->STATUS.reg & ADC_STATUS_SYNCBUSY) {
        return 1;
    }
    return 0;
}


int adc_mux_init_custom(void) {
	_prep();
	gpio_init (GPIO_PIN(PA, 13), GPIO_OUT);
	gpio_set (GPIO_PIN(PA, 13));
	_done();
	return 0;
}


int adc_gpio_init_custom(adc_t line)
{
    _prep();
    gpio_init(adc_channels[line].pin, GPIO_IN);
    gpio_init_mux(adc_channels[line].pin, GPIO_MUX_B);
    _done();
    return 0;
}


int adc_config_custom(adc_res_t res)
{
    _prep();

	// START CONGIF

    /* Disable ADC before reset */
    ADC_0_DEV->CTRLA.reg &= ~ADC_CTRLA_ENABLE;
	while (_adc_syncing()) {};

	/* Reset all ADC settings */
	//ADC_0_DEV->CTRLA.reg |= 0x01;
    //while (_adc_syncing()) {}

	/* Power On - enable ADC APB clock */
	PM->APBCMASK.reg |= PM_APBCMASK_ADC;

	/* GCLK Setup
	 * Bind a generic clock to ADC clock
	 * Enable generic clock 0x4000
	 * 'Generic clock generator 0' 0x0 << 8
	 * Enable ADC generic clock 0x1E */
	GCLK->CLKCTRL.reg = (uint32_t)(GCLK_CLKCTRL_CLKEN |
						GCLK_CLKCTRL_GEN_GCLK0 |
						(GCLK_CLKCTRL_ID(ADC_GCLK_ID)));

	/* Configure CTRLB Register HERE IS THE RESOLUTION SET! */
	ADC_0_DEV->CTRLB.reg = ADC_0_PRESCALER | res;
    while (_adc_syncing()) {}

	/* Load the fixed device calibration constants */
	ADC_0_DEV->CALIB.reg =
		ADC_CALIB_BIAS_CAL((*(uint32_t*)ADC_FUSES_BIASCAL_ADDR >>
							ADC_FUSES_BIASCAL_Pos)) |
		ADC_CALIB_LINEARITY_CAL((*(uint64_t*)ADC_FUSES_LINEARITY_0_ADDR >>
								ADC_FUSES_LINEARITY_0_Pos));

	/* Set Voltage Reference
	 * Set to VREFA = PA03, on Rhomb Zero connected to reference voltage */
	ADC_0_DEV->REFCTRL.reg = 0x3;
	//ADC_0_DEV->REFCTRL.reg = ADC_0_REF_DEFAULT;

	/* Disable all interrupts */
	ADC_0_DEV->INTENCLR.reg = (ADC_INTENCLR_SYNCRDY) | (ADC_INTENCLR_WINMON) |
							  (ADC_INTENCLR_OVERRUN) | (ADC_INTENCLR_RESRDY);

    /* Disable ADC in sleep mode (ADC config. remains after wakeup) */
	ADC_0_DEV->CTRLA.reg &= ~ADC_CTRLA_RUNSTDBY;

	/*  Enable ADC Module */
	ADC_0_DEV->CTRLA.reg |= ADC_CTRLA_ENABLE;
	while (_adc_syncing()) {}
	adc_state = ADC_STATE_ENABLED;

	/* Start a dummy conversion
	 * The first conversion after the reference is changed must not be used */
    ADC_0_DEV->SWTRIG.reg = ADC_SWTRIG_START;
    while (_adc_syncing()) {}

    /* Wait for the result */
    while (!(ADC_0_DEV->INTFLAG.reg & ADC_INTFLAG_RESRDY)) {}
    uint32_t result = ADC_0_DEV->RESULT.reg;

    /* Disable ADC after finishing */
	//ADC_0_DEV->CTRLA.reg &= ~ADC_CTRLA_ENABLE;
	//while (_adc_syncing()) {};
	//adc_state = ADC_STATE_DISABLED;

    _done();
    return 0;
}


int adc_sample_blocking_custom(adc_t line)
{
    _prep();

    /*  Enable ADC Module */
	//ADC_0_DEV->CTRLA.reg |= ADC_CTRLA_ENABLE;
	//while (_adc_syncing()) {}

	/* Set the gain and MUX POSITION for individual measurement.
	 * Gain = 1
	 * ADC pin = adc_channels[line].muxpos
	 * Negative input = internal GND */
    ADC_0_DEV->INPUTCTRL.reg = ADC_0_GAIN_FACTOR_DEFAULT |
                               adc_channels[line].muxpos |
							   ADC_0_NEG_INPUT;
    while (_adc_syncing()) {}

    /* Start the conversion */
    ADC_0_DEV->SWTRIG.reg = ADC_SWTRIG_START;
    while (_adc_syncing()) {}

    /* Wait for the result */
    while (!(ADC_0_DEV->INTFLAG.reg & ADC_INTFLAG_RESRDY)) {}
    int result = ADC_0_DEV->RESULT.reg;

    /* Disable ADC after finishing */
    //ADC_0_DEV->CTRLA.reg &= ~ADC_CTRLA_ENABLE;
	//while (_adc_syncing()) {};

    _done();
    return result;
}


int adc_sample_custom(adc_t line)
{
    _prep();

    if (_adc_syncing()) {
    	return -1;
    }

    switch (adc_state) {
    case ADC_STATE_DISABLED:
        /*  Enable ADC Module */
    	ADC_0_DEV->CTRLA.reg |= ADC_CTRLA_ENABLE;
    	adc_state = ADC_STATE_ENABLED;
    	break;
    case ADC_STATE_ENABLED:
    	/* Set the gain and MUX POSITION for individual measurement.
		 * Gain = 1
		 * ADC pin = adc_channels[line].muxpos
		 * Negative input = internal GND */
		ADC_0_DEV->INPUTCTRL.reg = ADC_0_GAIN_FACTOR_DEFAULT |
								   adc_channels[line].muxpos |
								   ADC_0_NEG_INPUT;
    	adc_state = ADC_STATE_INPUT_SELECTED;
    	break;
    case ADC_STATE_INPUT_SELECTED:
        /* Start the conversion */
        ADC_0_DEV->SWTRIG.reg = ADC_SWTRIG_START;
    	adc_state = ADC_STATE_CONVERSION_STARTED;
    	break;
    case ADC_STATE_CONVERSION_STARTED:
    	/* Wait for the result */
        if (ADC_0_DEV->INTFLAG.reg & ADC_INTFLAG_RESRDY) {
        	int result = ADC_0_DEV->RESULT.reg;
        	/* Disable ADC after finishing */
        	adc_state = ADC_STATE_DISABLED;
        	ADC_0_DEV->CTRLA.reg &= ~ADC_CTRLA_ENABLE;
        	return result;
        }
    	break;
    }

    _done();
    return -1;
}
